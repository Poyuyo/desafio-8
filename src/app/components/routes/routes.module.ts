import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio/inicio.component';
import { CardsComponent } from './cards/cards.component';
import { ContactoComponent } from './contacto/contacto.component';



@NgModule({
  declarations: [
    InicioComponent,
    CardsComponent,
    ContactoComponent
  ],
  imports: [
    CommonModule
  ]
})
export class RoutesModule { }
